Sandbox repo to investigate options for Drupal + Kubernetes.

Sample deployment files based on https://www.linode.com/docs/guides/how-to-install-drupal-with-linode-kubernetes-engine/

# Installation

## Add the nginx ingress controller

https://kubernetes.github.io/ingress-nginx/deploy/

Old
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/cloud/deploy.yaml
```

New
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.45.0/deploy/static/provider/cloud/deploy.yaml
```

## Clone the repo

```
git clone git@gitlab.com:Asacolips/drupal-kubernetes.git
cd drupal-kubernetes
```

## Initialize persistent volumes

```
kubectl apply -f ./volumes.yaml
```

## Start the service

```
kubectl apply -k ./
```

Database credentials:

- **Database**: `drupal-db`
- **User**: `root`
- **Pass**: `MySQLpassword`
- **Host**: `drupal-mysql`

## Stop the service

```
kubectl delete -k ./
```

## Delete the persistent volumes

```
kubectl delete -f ./volumes.yaml
```

---

# Minikube setup

## Install Minikube

```
brew install minikube
brew link minkube
```

## Start Minikube and Nginx Ingress

```
minikube start --vm-driver=virtualbox
minikube addons enable ingress
```

## Start the dashboard

```
minikube dashboard
```

## Create the volumes

```
kubectl apply -f ./volumes-minikube.yaml
```

## Start the Drupal + MySQL deployment

```
kubectl apply -k ./
```

## Add the domain to your hosts file

1. Head to the Kubernetes Dashboard created by Minikube earlier, and look under **Services** > **Ingresses** for your `ingress-drupal-host` service. The IP address under the _Endpoints_ column is your local cluster's IP used for the ingress - copy that for the next step.
2. Edit your hosts file `/etc/hosts` and add the following: `192.168.99.102 drupal.k8s.site.local`, substituting the IP address with the one from the previous step. The `drupal.k8s.site.local` domain is the one the deployment is configured to use, which can be changed in `drupal-deployment.yaml`

## Teardown

```
kubectl delete -k ./
kubectl delete -f ./volumes-minikube.yaml
minikube stop
minikube delete
```